# Apery-Evaluation-Binaries-2019-02-24

Use these evaluation binaries with Apery.

The repository of Apery
[https://github.com/HiraokaTakuya/apery.git](https://github.com/HiraokaTakuya/apery.git)


## License

This is distributed under the MIT license.

See LICENSE-MIT for details.
